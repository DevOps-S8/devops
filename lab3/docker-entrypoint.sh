#!/bin/sh

while ! nc -z db 5432; do
	sleep 0.1
done

python manage.py migrate

python manage.py collectstatic --noinput

python manage.py createsuperuser --noinput --username admin --email admin@example.com

exec gunicorn --bind :80 --workers 3 locallibrary.wsgi:application
