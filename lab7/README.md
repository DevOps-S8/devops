## Spring boot progect 
Source code get from https://github.com/spring-guides/gs-spring-boot

### Build image
```bash
sudo docker-compose run spring
```
### Start app
```bash
sudo docker-compose up -d
```
### Stop
```bash
sudo docker-compose stop 
```